package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity

@Table(name="users")

public class User {

    //for the id that auto increments
    @Id
    @GeneratedValue
    private Long id;

    //table column headers
    @Column
    private String username;
    @Column
    private String password;

   @OneToMany(mappedBy = "user")
   //to avoid infinite recursion, use the @JsonIgnore annotation
    @JsonIgnore
    private Set<Post> posts;

    public Set<Post> getPosts() {
        return posts;
    }


    //Default constructor
    public User(){}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    // setters and getters
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
