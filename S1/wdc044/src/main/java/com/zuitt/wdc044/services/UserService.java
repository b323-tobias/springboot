package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

//prevents a null pointer
//method may or may not return a value
//return will be an optional object
import java.util.Optional;

public interface UserService {
    void createUser (User user);

    Optional<User> findByUsername(String username);

}
