package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.lang.Iterable;

@RestController
@CrossOrigin
//Enable cross origin requests via @CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> getAllPosts() {
        Iterable<Post> iterablePosts = postService.getPosts();
        List<Post> posts = new ArrayList<>();
        iterablePosts.forEach(posts::add);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }
}
